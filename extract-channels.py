import wavio
from sys import argv
from os import path, sep

input_file_path = path.abspath(argv[1])
output_path = path.abspath(argv[2])
file_name = path.basename(input_file_path)

File = wavio.read(input_file_path)
rate = File.rate

x = 0
while(x < len(File.data[0])):
    channel = File.data[:,x]
    wavio.write(f"{output_path}{sep}{file_name}_channel_{x+1}.wav", channel, rate)
    print(f"Channel({x+1}) extracted successfully.")
    x += 1